// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ASSIGNMENT2_Assignment2GameModeBase_generated_h
#error "Assignment2GameModeBase.generated.h already included, missing '#pragma once' in Assignment2GameModeBase.h"
#endif
#define ASSIGNMENT2_Assignment2GameModeBase_generated_h

#define Assignment2_Source_Assignment2_Assignment2GameModeBase_h_15_SPARSE_DATA
#define Assignment2_Source_Assignment2_Assignment2GameModeBase_h_15_RPC_WRAPPERS
#define Assignment2_Source_Assignment2_Assignment2GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Assignment2_Source_Assignment2_Assignment2GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAssignment2GameModeBase(); \
	friend struct Z_Construct_UClass_AAssignment2GameModeBase_Statics; \
public: \
	DECLARE_CLASS(AAssignment2GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Assignment2"), NO_API) \
	DECLARE_SERIALIZER(AAssignment2GameModeBase)


#define Assignment2_Source_Assignment2_Assignment2GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAAssignment2GameModeBase(); \
	friend struct Z_Construct_UClass_AAssignment2GameModeBase_Statics; \
public: \
	DECLARE_CLASS(AAssignment2GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Assignment2"), NO_API) \
	DECLARE_SERIALIZER(AAssignment2GameModeBase)


#define Assignment2_Source_Assignment2_Assignment2GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAssignment2GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAssignment2GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAssignment2GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAssignment2GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAssignment2GameModeBase(AAssignment2GameModeBase&&); \
	NO_API AAssignment2GameModeBase(const AAssignment2GameModeBase&); \
public:


#define Assignment2_Source_Assignment2_Assignment2GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAssignment2GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAssignment2GameModeBase(AAssignment2GameModeBase&&); \
	NO_API AAssignment2GameModeBase(const AAssignment2GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAssignment2GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAssignment2GameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAssignment2GameModeBase)


#define Assignment2_Source_Assignment2_Assignment2GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define Assignment2_Source_Assignment2_Assignment2GameModeBase_h_12_PROLOG
#define Assignment2_Source_Assignment2_Assignment2GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Assignment2_Source_Assignment2_Assignment2GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Assignment2_Source_Assignment2_Assignment2GameModeBase_h_15_SPARSE_DATA \
	Assignment2_Source_Assignment2_Assignment2GameModeBase_h_15_RPC_WRAPPERS \
	Assignment2_Source_Assignment2_Assignment2GameModeBase_h_15_INCLASS \
	Assignment2_Source_Assignment2_Assignment2GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Assignment2_Source_Assignment2_Assignment2GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Assignment2_Source_Assignment2_Assignment2GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Assignment2_Source_Assignment2_Assignment2GameModeBase_h_15_SPARSE_DATA \
	Assignment2_Source_Assignment2_Assignment2GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Assignment2_Source_Assignment2_Assignment2GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	Assignment2_Source_Assignment2_Assignment2GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ASSIGNMENT2_API UClass* StaticClass<class AAssignment2GameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Assignment2_Source_Assignment2_Assignment2GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
